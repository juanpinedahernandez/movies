package com.udacity.movies;

import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class DetailsActivity extends ActionBarActivity {
    private static final int ANIM_DURATION = 600;
    private TextView titleTextView,yearTextView,descriptionTextView,voteTextView;
    private ImageView imageView;

    private int mLeftDelta;
    private int mTopDelta;
    private float mWidthScale;
    private float mHeightScale;

    private LinearLayout linearLayout;
    private ColorDrawable colorDrawable;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Setting details screen layout
        setContentView(R.layout.activity_details_view);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        //retrieves the thumbnail data
        Bundle bundle = getIntent().getExtras();
        String title = bundle.getString("title");
        String image = bundle.getString("image");
        String vote = bundle.getString("vote");
        String description = bundle.getString("description");
        String year = bundle.getString("year");


        //initialize and set the image description
        titleTextView = (TextView) findViewById(R.id.title);
        voteTextView = (TextView) findViewById(R.id.vote);
        descriptionTextView = (TextView) findViewById(R.id.description);
        yearTextView = (TextView) findViewById(R.id.year);
        titleTextView.setText(Html.fromHtml(title));
        voteTextView.setText(Html.fromHtml(vote)+"/10");
        descriptionTextView.setText(Html.fromHtml(description));
        yearTextView.setText(Html.fromHtml(year));

        //Set image url
        imageView = (ImageView) findViewById(R.id.grid_item_image);
        Picasso.with(this).load(image).into(imageView);




    }


}
